#!/bin/bash
declare -a Softs
Softs=(composer clementine chromium-browser flatpak gnome-software-plugin-flatpak
    libreoffice gparted gpodder htop inkscape gimp docker.io docker-compose git
    gnome-disk-utility lm-sensors lame chrome-gnome-shell filezilla bleachbit
    mkchromecast mkchromecast-pulseaudio wget curl timeshift nextcloud-client
    nodejs npm
)
declare -a SoftsForRip
SoftsForRip=(asunder picard)

function testAndInstallRepository() {
    if ! grep -q "^deb .*$1" /etc/apt/sources.list /etc/apt/sources.list.d/*; then
        echo "installing repo $1"
        sudo add-apt-repository -y ppa:$1
    else
        echo "Repositoty $1 already exist"
    fi
}

function askInstallHandbrake() {
    echo "install handbrake ? (N/y)"
    read instHandbrk
    if [[ "$instHandbrk" == 'y' || "$instHandbrk" == 'Y' ]]; then
        testAndInstallRepository "stebbins/handbrake-releases"
        Softs+=(handbrake-gtk)
        Softs+=(libdvd-pkg)
        handbrake="yes"
    else
        echo "Install Handbrake Aborded"
    fi
}

function updateAll() {
    sudo apt update && sudo apt upgrade -y
}

function askInstallMolotov() {
    echo "Install Molotov ? (N/y)"
    read instMolotov
    if [[ "$instMolotov" == 'y' || "$instMolotov" == 'Y' ]]; then
        molotovURL=http://desktop-auto-upgrade.molotov.tv/linux/3.1.0/molotov.AppImage
        if [ -d "/opt/molotov" ]; then
            echo "Directory /opt/molotov already exist"
        else
            sudo mkdir /opt/molotov
            sudo chmod 755 /opt/molotov
            sudo wget $molotovURL -O /opt/molotov/molotov.AppImage
            sudo chmod +x /opt/molotov/molotov.AppImage
            cd /opt/molotov && ./molotov.AppImage
            echo "Hit enter to continue" && read
        fi
    elif [[ "$instMolotov" == 'n' || "$instMolotov" == 'N' || "$instMolotov" == "" ]]; then
        echo "Molotov Install Aborded"
    fi
}

function addRepository() {
    # NextCloud
    testAndInstallRepository "nextcloud-devs/client"
    # Flatpack
    testAndInstallRepository "alexlarsson/flatpak"
    # TimeShift & apttik
    testAndInstallRepository "teejee2008/ppa"
}

function askInstallCode() {
    echo "Install & configure Code ? (N/y)" && read instVsCode
    if [[ "$instVsCode" == 'y' || "$instVsCode" == 'Y' ]]; then
    	sudo apt-key adv --keyserver keyserver.ubuntu.com --recv-keys EB3E94ADBE1229CF
		sudo add-apt-repository -y "deb [arch=amd64] https://packages.microsoft.com/repos/vscode stable main"
        # curl https://packages.microsoft.com/keys/microsoft.asc | gpg --dearmor >microsoft.gpg
        # sudo install -o root -g root -m 644 microsoft.gpg /etc/apt/trusted.gpg.d/
        # sudo sh -c 'echo "deb [arch=amd64] https://packages.microsoft.com/repos/vscode stable main" > /etc/apt/sources.list.d/vscode.list'
        Softs+=(apt-transport-https code)
    elif [[ "$instVsCode" == 'n' || "$instVsCode" == 'N' || "$instVsCode" == '' ]]; then
        echo "Not install Code"
    fi
}

function askInstallAptSoftsList() {
    echo "Install Apt Softs List ? (N/y)"
    read instAptSoftList
    if [[ "$instAptSoftList" == 'y' || "$instAptSoftList" == 'Y' ]]; then
        for i in ${Softs[@]}; do
            echo "Installing..." $i
            sudo apt -y install $i
        done
    elif [[ "$instAptSoftList" == 'n' || "$instAptSoftList" == 'N' || "$instAptSoftList" == '' ]]; then
        echo "Install Apt Softs List Aborded"
    fi
}

function askInstallSoftsForRip() {
    echo "Install Apt Softs for rip List ? (N/y)"
    read SoftsForRip
    if [[ "$SoftsForRip" == 'y' || "$SoftsForRip" == 'Y' ]]; then
        for i in ${SoftsForRip[@]}; do
            echo "Installing..." $i
            sudo apt -y install $i
        done
    elif [[ "$SoftsForRip" == 'n' || "$SoftsForRip" == 'N' || "$SoftsForRip" == '' ]]; then
        echo "Install Apt Softs List Aborded"
    fi
}

function setupLibdvd() {
    if [[ "$instHandbrk" == 'y' || "$instHandbrk" == 'Y' ]]; then
        sudo dpkg-reconfigure libdvd-pkg
    fi
}

function setupGit() {
    echo "Semi-automatic git setup (N/y) ?" && read settingUpGit
    if [[ "$settingUpGit" == 'y' || "$settingUpGit" == 'Y' ]]; then
        echo "Type in your first and last name (no accent or special characters - e.g. 'ç'): "
        read full_name
        echo "Type in your email address: "
        read email
        echo "Type in cache timeout (in seconde)(3600 is good idea): "
        read timeout
        git config --global user.email $email
        git config --global user.name "$full_name"
        git config --global credential.helper 'cache --timeout=$timeout'
    elif [[ "$settingUpGit" == 'n' || "$settingUpGit" == 'N' || "$settingUpGit" == "" ]]; then
        echo "Git setup Aborded"

    fi
}

function setupDocker() {
    # Add $USER to docker group
    echo "Adding current USER to docker group"
    sudo groupadd docker
    sudo usermod -aG docker $USER
}

function setupFlatpakRepo() {
    # Add flathub repo
    echo "Adding flathub repo"
    flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo
}

function setupVSCode() {
    echo "Enter the local user login"
    read localUserLogin
    # Installing Sync plugin
    sudo -H -u $localUserLogin bash -c 'code --install-extension Shan.code-settings-sync'
    # Installing Wakatime plugin
    sudo -H -u $localUserLogin bash -c 'code --install-extension wakatime.vscode-wakatime'
    # Installing language pack french plugin
    sudo -H -u $localUserLogin bash -c 'code --install-extension ms-ceintl.vscode-language-pack-fr'
    # Installing docker plugin
    sudo -H -u $localUserLogin bash -c 'code --install-extension ms-azuretools.vscode-docker'

}

function installSnapSofts() {
    # install gravit-designer snap
    echo "installing Gravit Designer's snap"
    sudo snap install gravit-designer
    # install Wekan snap
    echo "installing Wekan's snap"
    sudo snap install wekan
    sudo snap set wekan port='5000'
}

function askAndInstallFirefoxDev() {
    echo "Install Firefox Dev Edition (N/y)"
    read instFirefoxDev
    if [[ "$instFirefoxDev" == 'y' || "$instFirefoxDev" == 'Y' ]]; then
        sudo mkdir /opt/firefox
        wget -O /opt/firefox/installfirefox.tar.bz2 "https://download.mozilla.org/?product=firefox-devedition-latest-ssl&os=linux64&lang=fr"
        
        sudo tar xjf /opt/firefox/installfirefox.tar.bz2 -C /opt/firefox/
        sudo cp ./asset/firefox-devedition.desktop /usr/share/applications/firefox-devedition.desktop
        sudo chown -R $USER:$USER /opt/firefox && sudo chown root:root /usr/share/applications/firefox-devedition.desktop
    elif [[ "$instFirefoxDev" == 'n' || "$instFirefoxDev" == 'N' || "$instFirefoxDev" == "" ]]; then
        echo "Install Firefox Dev Edition aborded"
    fi
}

# --------------------------------------- BACKUP FUNCTIONS --------------------------------------

function backupFirefox() {
    FILENAME=$(basename $(ls -d $HOME/.mozilla/firefox/*.default | sort))
    echo $FILENAME
    tar czf ./backups/firefoxBackup/$FILENAME.tar.gz -C $HOME/.mozilla/firefox/*.default .
}

function backupFirefoxDev() {
    FILENAME=$(basename $(ls -d $HOME/.mozilla/firefox/*.dev-edition-default | sort))
    echo $FILENAME
    tar czf ./backups/firefoxBackup/$FILENAME.tar.gz -C $HOME/.mozilla/firefox/*.dev-edition-default .
}

function backupDotFiles() {
    cp ~/{.bashrc,.gitconfig} ./backups/dotfiles/
}

# --------------------------------------- MAIN RESTORE FUNCTION --------------------------------------

function installAll() {
    handbrake="no"

    updateAll

    askInstallHandbrake
    askInstallCode
    askInstallMolotov
    askAndInstallFirefoxDev
    addRepository
    apt upgrade
    askInstallAptSoftsList
    askInstallSoftsForRip

    installSnapSofts

    setting up libdvd
    if [[ "$handbrake" == 'yes' ]]; then
        setupLibdvd
    fi
    setupDocker
    setupFlatpakRepo
    setupGit
    setupVSCode
    echo "End of softwares restauration, hit enter key to continue..." && read
    clear
}

# --------------------------------------- MAIN BACKUP FUNCTION -------------------------------

function backupFuntion() {
    # ---- Backup Firefox ----
    echo "Backup Firefox default profil ? (N/y)"
    read backupFirefox
    if [[ "$backupFirefox" == 'y' || "$backupFirefox" == 'Y' ]]; then
        backupFirefox
    fi
    # ---- Backup Firefox Developper Edition ----
    echo "Backup Firefox Developper Edition default profil ? (N/y)"
    read backupFirefoxDev
    if [[ "$backupFirefoxDev" == 'y' || "$backupFirefoxDev" == 'Y' ]]; then
        backupFirefoxDev
    fi
    # ---- Backup dotfiles ----
    echo "Backup dotfiles ? (N/y)"
    read backupDotFiles
    if [[ "$backupDotFiles" == 'y' || "$backupDotFiles" == 'Y' ]]; then
        backupDotFiles
    fi
}

# --------------------------------------- MENU FUNCTION --------------------------------------

function main() {
    echo "Restore or Backup ? (Type 'Rest' or 'Back')"
    echo "Type q for quit"
    read restOrBackup
    if [[ "$restOrBackup" == 'Rest' || "$restOrBackup" == 'rest' ]]; then
        clear
        installAll
    elif [[ "$restOrBackup" == 'Back' || "$restOrBackup" == 'back' ]]; then
        clear
        backupFuntion
    elif [[ "$restOrBackup" == 'Q' || "$restOrBackup" == 'q' ]]; then
        echo "See you"
    else
        echo "Type q for quit"
    fi
}

# --------------------------------------- MAIN LOOP --------------------------------------

until [[ "$restOrBackup" == "q" || "$restOrBackup" == "Q" ]]; do
    main
done
#
# Applis: VSCode sauvegarde/restauration
# Faire des logs
# Sauve dans le cloud/mail ?
